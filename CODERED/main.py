import tkinter as tk
from PIL import Image, ImageTk

def resize_image(image_path, width, height):
    image = Image.open(image_path)
    image = image.resize((width, height))
    return ImageTk.PhotoImage(image)

root = tk.Tk()
root.title("CodeRed")
def readfile():
	with open('testfile', 'r') as file:
		line = file.readline()
		line = line.strip()
		bytes_list = []
		for i in range(0, len(line), 8):
			byte_str = line[i:i+8]
			byte_int = int(byte_str, 2)
			bytes_list.append(byte_int)
	return bytes_list
# Function to create the home screen
def temp(offset):
	return tk.Button(root, text="TEMP SENSOR: \n "+ ''.join(map(str, readfile()[offset:offset+2])) , font=("Monospace", 10), bg="#00FF00",padx=300, pady=400)
def pressure(offset):
	return tk.Button(root, text="PRESSURE SENSOR: \n "+ ''.join(map(str, readfile()[offset:offset+1])), font=("Monospace",10), bg="#00FF00", padx=300,pady=400) 
def acel(offset):
	return tk.Button(root, text="ACCELEROMETER SENSOR: \n" + ''.join(map(str, readfile()[offset:offset+3])), font=("Monospace",10),bg="#00FF00", padx=300,pady=400)
def homescreen():
    # Remove any existing elements
    clear_widgets()

    # Create buttons for each option
    button1 = tk.Button(root, image=image1, command=rocket)
    button1.grid(row=0, column=0, padx=10, pady=10)
    button1.config(width=900, height=900)

    button2 = tk.Button(root, image=image2, command=drill)
    button2.grid(row=0, column=1, padx=10, pady=10)
    button2.config(width=900, height=900)

    button3 = tk.Button(root, image=image3, command=drone)
    button3.grid(row=0, column=2, padx=10, pady=10)
    button3.config(width=900, height=900)

    button4 = tk.Button(root, image=image4, command=cnc)
    button4.grid(row=0, column=3, padx=10, pady=10)
    button4.config(width=900, height=900)

    button5 = tk.Button(root, image=image5, command=td)
    button5.grid(row=1, column=0, padx=10, pady=10)
    button5.config(width=900, height=900)

    button6 = tk.Button(root, image=image6, command=pipe)
    button6.grid(row=1, column=1, padx=10, pady=10)
    button6.config(width=900, height=900)

    button7 = tk.Button(root, image=image8, command=ekg)
    button7.grid(row=1, column=2, padx=10, pady=10)
    button7.config(width=900, height=900)

    button8 = tk.Button(root, image=image7, command=sensors)
    button8.grid(row=1, column=3, padx=10, pady=10)
    button8.config(width=900, height=900)

# Function to create the sensors screen
def rocket():
	clear_widgets()
	sensors = [temp, pressure, acel]
	button1 = sensors[0](0)
	button1.grid(row=0, column=1, padx=10, pady=10)

	button2 = tk.Button(root, text="Back",font=("Monospace",20), padx=400,pady=400, command=homescreen)
	button2.grid(row=0, column=0, padx=10, pady=10)

	button3 = tk.Button(root, text="ROCKET SENSOR 2", font=("Monospace", 10), bg="#00FF00",padx=300, pady=420)
	button3.grid(row=0, column=2, padx=10, pady=10)
	
	button4 = tk.Button(root, text="ROCKET SENSOR 3", font=("Monospace", 10), bg="red",padx=300, pady=420)
	button4.grid(row=0, column=3, padx=10, pady=10)
	
	button5 = tk.Button(root, text="ROCKET SENSOR 4", font=("Monospace", 10), bg="#00FF00",padx=300, pady=420)
	button5.grid(row=1, column=0, padx=10, pady=10)
def drill():
	clear_widgets()
	
	button1 = tk.Button(root, text="DRILL SENSOR", font=("Monospace", 10), bg="#00FF00",padx=300, pady=420)
	button1.grid(row=0, column=1, padx=10, pady=10)

	button2 = tk.Button(root, text="Back",font=("Monospace",20), padx=400,pady=400, command=homescreen)
	button2.grid(row=0, column=0, padx=10, pady=10)

	button3 = tk.Button(root, text="DRILL SENSOR 2", font=("Monospace", 10),bg="#00FF00", padx=300, pady=420)
	button3.grid(row=0, column=2, padx=10, pady=10)

	button4 = tk.Button(root, text="DRILL SENSOR 3", font=("Monospace", 10), bg="#00FF00", padx=300, pady=420)
	button4.grid(row=0, column=3, padx=10, pady=10)

	button5 = tk.Button(root, text="DRILL SENSOR 4", font=("Monospace", 10), bg="red",padx=300, pady=420)
	button5.grid(row=1, column=0, padx=10, pady=10)

	button6 = tk.Button(root, text="DRILL SENSOR 5", font=("Monospace", 10), bg="#00FF00", padx=300, pady=420)
	button6.grid(row=1, column=1, padx=10, pady=10)
def drone():
	clear_widgets()
	
	button1 = tk.Button(root, text="DRONE SENSOR", font=("Monospace", 10), bg="#00FF00", padx=300, pady=420)
	button1.grid(row=0, column=1, padx=10, pady=10)

	button2 = tk.Button(root, text="Back",font=("Monospace",20), padx=400,pady=400, command=homescreen)
	button2.grid(row=0, column=0, padx=10, pady=10)

	button3 = tk.Button(root, text="ROCKET SENSOR 2", font=("Monospace", 10), bg="#00FF00", padx=300, pady=420)
	button3.grid(row=0, column=2, padx=10, pady=10)

	button4 = tk.Button(root, text="ROCKET SENSOR 3", font=("Monospace", 10), bg="#00FF00",padx=300, pady=420)
	button4.grid(row=0, column=3, padx=10, pady=10)
	
	button5 = tk.Button(root, text="ROCKET SENSOR 4", font=("Monospace", 10), bg="red", padx=300, pady=420)
	button5.grid(row=1, column=0, padx=10, pady=10)

	button6 = tk.Button(root, text="ROCKET SENSOR 5", font=("Monospace", 10), bg="#00FF00", padx=300, pady=420)
	button6.grid(row=1, column=1, padx=10, pady=10)

	button7 = tk.Button(root, text="ROCKET SENSOR 6", font=("Monospace", 10), bg="#00FF00",padx=300, pady=420)
	button7.grid(row=1, column=2, padx=10, pady=10)

	button8 = tk.Button(root, text="ROCKET SENSOR 7", font=("Monospace", 10), bg="#00FF00", padx=300, pady=420)
	button8.grid(row=1, column=3, padx=10, pady=10)
def cnc():
	clear_widgets()
	
	button1 = tk.Button(root, text="CNC SENSOR", font=("Monospace", 10), bg="#00FF00", padx=300, pady=420)
	button1.grid(row=0, column=1, padx=10, pady=10)

	button2 = tk.Button(root, text="Back",font=("Monospace",20), padx=400,pady=400, command=homescreen)
	button2.grid(row=0, column=0, padx=10, pady=10)
	
	button3 = tk.Button(root, text="CNC SENSOR 2", font=("Monospace", 10), bg="#00FF00", padx=300, pady=420)
	button3.grid(row=0, column=2, padx=10, pady=10)

	button4 = tk.Button(root, text="CNC SENSOR 3", font=("Monospace", 10), bg="#00FF00", padx=300, pady=420)
	button4.grid(row=0, column=3, padx=10, pady=10)

	button5 = tk.Button(root, text="CNC SENSOR 4", font=("Monospace", 10), bg="#00FF00", padx=300, pady=420)
	button5.grid(row=1, column=0, padx=10, pady=10)
	
	button6 = tk.Button(root, text="CNC SENSOR 5", font=("Monospace", 10), bg="#00FF00", padx=300, pady=420)
	button6.grid(row=1, column=1, padx=10, pady=10)
def td():
	clear_widgets()
	
	button1 = tk.Button(root, text="3D SENSOR", font=("Monospace", 10), bg="#00FF00", padx=300, pady=420)
	button1.grid(row=0, column=1, padx=10, pady=10)

	button2 = tk.Button(root, text="Back",font=("Monospace",20), padx=400,pady=400, command=homescreen)
	button2.grid(row=0, column=0, padx=10, pady=10)

	
def pipe():
	clear_widgets()
	
	button1 = tk.Button(root, text="PIPE SENSOR", font=("Monospace", 10), bg="#00FF00" ,padx=300, pady=420)
	button1.grid(row=0, column=1, padx=10, pady=10)

	button2 = tk.Button(root, text="Back",font=("Monospace",20), padx=400,pady=400, command=homescreen)
	button2.grid(row=0, column=0, padx=10, pady=10)
	
	button3 = tk.Button(root, text="PIPE SENSOR 2", font=("Monospace", 10), bg="#00FF00" ,padx=300, pady=420)
	button3.grid(row=0, column=2, padx=10, pady=10)

	button4 = tk.Button(root, text="PIPE SENSOR 3", font=("Monospace", 10), bg="#00FF00", padx=300, pady=420)
	button4.grid(row=0, column=3, padx=10, pady=10)

	button5 = tk.Button(root, text="PIPE SENSOR 4", font=("Monospace", 10), bg="#00FF00",padx=300, pady=420)
	button5.grid(row=1, column=0, padx=10, pady=10)

	button6 = tk.Button(root, text="PIPE SENSOR 5", font=("Monospace", 10), bg="#00FF00", padx=300, pady=420)
	button6.grid(row=1, column=1, padx=10, pady=10)
def ekg():
	clear_widgets()
	
	button1 = tk.Button(root, text="EKG SENSOR", font=("Monospace", 10), bg="#00FF00", padx=300, pady=420)
	button1.grid(row=0, column=1, padx=10, pady=10)

	button2 = tk.Button(root, text="Back",font=("Monospace",20), padx=400,pady=400, command=homescreen)
	button2.grid(row=0, column=0, padx=10, pady=10)
	
	button3 = tk.Button(root, text="EKG SENSOR 2", font=("Monospace", 10), bg="#00FF00", padx=300, pady=420)
	button3.grid(row=0, column=2, padx=10, pady=10)

	button3 = tk.Button(root, text="EKG SENSOR 3", font=("Monospace", 10), bg="#00FF00",padx=300, pady=420)
	button3.grid(row=0, column=3, padx=10, pady=10)

	button3 = tk.Button(root, text="EKG SENSOR 4", font=("Monospace", 10), bg="#00FF00",padx=300, pady=420)
	button3.grid(row=1, column=0, padx=10, pady=10)

	button3 = tk.Button(root, text="EKG SENSOR 5", font=("Monospace", 10),bg="#00FF00", padx=300, pady=420)
	button3.grid(row=1, column=1, padx=10, pady=10)

	button3 = tk.Button(root, text="EKG SENSOR 6", font=("Monospace", 10),bg="red", padx=300, pady=420)
	button3.grid(row=1, column=2, padx=10, pady=10)

	button3 = tk.Button(root, text="EKG SENSOR 7", font=("Monospace", 10),bg="#00FF00", padx=300, pady=420)
	button3.grid(row=1, column=3, padx=10, pady=10)
def sensors():
	clear_widgets()
	
	button1 = tk.Button(root, text="SENSOR EXAMPLE 1", font=("Monospace", 10), padx=300, pady=420)
	button1.grid(row=0, column=1, padx=10, pady=10)

	button2 = tk.Button(root, text="Back",font=("Monospace",20), padx=400,pady=400, command=homescreen)
	button2.grid(row=0, column=0, padx=10, pady=10)
# Function to clear all widgets from the window
def clear_widgets():
    for widget in root.winfo_children():
        widget.grid_forget()

# Load images
image1 = resize_image("rocket.jpg", 900, 900)
image2 = resize_image("drill.png", 900, 900)
image3 = resize_image("drone.png", 900, 900)
image4 = resize_image("cnc.png", 900, 900)
image5 = resize_image("3d.png", 900, 900)
image6 = resize_image("pipe.png", 900, 900)
image7 = resize_image("custom.png", 900, 900)
image8 = resize_image("ekg.png", 900, 900)
image9 = resize_image("back.png", 900, 900)

# Show the home screen initially
homescreen()

root.mainloop()
